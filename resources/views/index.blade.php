<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>G2G</title>
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/normalize.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/demo.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/ns-default.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/ns-style-attached.css')}}" />
    <script src="{{url('assets/js/modernizr.custom.js')}}"></script>
  </head>
  <body>
    <div class="container">
      <section class="content bgcolor-4">
				<h1>GitLab To GitLab</h1>

			</section>
    <section class="content bgcolor-1">
      <h3>from</h3>
      <span class="input input--nao">
        <input class="input__field input__field--nao" type="text" id="input-1" />
        <label class="input__label input__label--nao" for="input-1">
          <span class="input__label-content input__label-content--nao">Private Token</span>
        </label>
        <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
          <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
        </svg>
      </span>
      <span class="input input--nao">
        <input class="input__field input__field--nao" type="text" id="input-2" />
        <label class="input__label input__label--nao" for="input-2">
          <span class="input__label-content input__label-content--nao">Username</span>
        </label>
        <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
          <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
        </svg>
      </span>
      <span class="input input--nao">
        <input class="input__field input__field--nao" type="password" id="input-3" />
        <label class="input__label input__label--nao" for="input-3">
          <span class="input__label-content input__label-content--nao">Password</span>
        </label>
        <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
          <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
        </svg>
      </span>
    </section>
    <section class="content">
      <h3>to</h3>
      <span class="input input--yoshiko">
        <input class="input__field input__field--yoshiko" type="text" id="input-12" />
        <label class="input__label input__label--yoshiko" for="input-12">
          <span class="input__label-content input__label-content--yoshiko" data-content="To Private Token">Private Token</span>
        </label>
      </span>
    </section>
    <section class="content bgcolor-5">
      <div role="button" tabindex="0" aria-label="Download" class="Download" data-progressbar-label="Downloading item..."></div>

    </section>
    <section class="content bgcolor-4">
      <h1>GitLab To GitLab</h1>
    </section>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.17.0/TweenMax.min.js"></script>
    <script src="{{url('assets/js/classie.js')}}"></script>
    <script src="{{url('assets/js/notificationFx.js')}}"></script>
		<script src="{{url('assets/js/elastic-progress.js')}}"></script>
		<!-- <script src="{{url('assets/js/main.js')}}"></script> -->

    <script type="text/javascript">
      $(document).ready(function() {

        // $('.Download').on('click', function(){

          

        // });
        var timing = false;
        $(".Download")
          .eq(0)
          .ElasticProgress({
            align: "center",
            colorFg: "#f9f8f7",
            colorBg: "#c5c5c5",
            highlightColor: "#ffab91",
            width: Math.min($(window).width() / 2 - 100, 600),
            barHeight: 10,
            labelHeight: 50,
            labelWobbliness: 150,
            bleedTop: 120,
            bleedRight: 100,
            buttonSize: 100,
            fontFamily: "Arvo",
            barStretch: 10,
            barInset: 4,
            barElasticOvershoot: 4,
            barElasticPeriod: 0.9,
            textFail: "Transfer Failed",
            textComplete: "Transfer Complete",
            arrowHangOnFail: true,
            arrowDirection: "up",
            onClick: function() {

              if($('body').find('span.icon-calendar').length > 0){
                $('body').find('.ns-show').remove();
              }

              var private_token = $('#input-1').val();
              
              if(!private_token){
                var notification = new NotificationFx({
                    message:
                      '<span class="icon icon-calendar"></span><p>Private Token is required!</p>',
                    layout: "attached",
                    effect: "bouncyflip",
                    type: "notice", // notice, warning or error
                    onClose: function() {
                      //bttn.disabled = false;
                    }
                  });

                notification.show();
                return false;
              }
              var username = $('#input-2').val();
              if(!username){
                var notification = new NotificationFx({
                    message:
                      '<span class="icon icon-calendar"></span><p>Username is required!</p>',
                    layout: "attached",
                    effect: "bouncyflip",
                    type: "notice", // notice, warning or error
                    onClose: function() {
                      //bttn.disabled = false;
                    }
                  });

                notification.show();
                return false;
              }
              var password = $('#input-3').val();
              if(!password){
                var notification = new NotificationFx({
                    message:
                      '<span class="icon icon-calendar"></span><p>Password is required!</p>',
                    layout: "attached",
                    effect: "bouncyflip",
                    type: "notice", // notice, warning or error
                    onClose: function() {
                      //bttn.disabled = false;
                    }
                  });

                notification.show();
                return false;
              }

              var second_private_token = $('#input-12').val();
              if(!second_private_token){
                var notification = new NotificationFx({
                    message:
                      '<span class="icon icon-calendar"></span><p>To Private Token is required!</p>',
                    layout: "attached",
                    effect: "bouncyflip",
                    type: "notice", // notice, warning or error
                    onClose: function() {
                      //bttn.disabled = false;
                    }
                  });

                notification.show();
                return false;
              }

              $(this).ElasticProgress("open");
              var data = 'from_token='+private_token+'&username='+username+'&password='+password+'&to_token='+second_private_token;
              $.ajax({
                  url: '{{url("gitlab/submit")}}',
                  type: 'get', // Send post data
                  data: data,
                  success: function(s){

                    if(s == 'Something Wrong!' || s != 'Successfully'){
                      var notification = new NotificationFx({
                          message:
                            '<span class="icon icon-calendar"></span><p>Something Wrong!</p>',
                          layout: "attached",
                          effect: "bouncyflip",
                          type: "notice", // notice, warning or error
                          
                          onClose: function() {
                            //bttn.disabled = false;
                          }
                        });

                      notification.show();
                    }


                    if($.isArray(s.result)){
                      console.log(s.result);
                      var list = s.result;
                      for (var i = 0; i < 2; i++) {
                        
                        gitLabAjax(s.username, s.password, s.from_token, s.to_token, s.result[i]);

                      }
                    }

                  
                    timing = true;

                  }
              });


            },
            onOpen: function() {
              fakeLoading($(this));
            },
            onComplete: function() {
              var $obj = $(this);

              TweenMax.delayedCall(1.5, function() {
                $obj.ElasticProgress("close");
              });
            }
          });

        function fakeLoading($obj, speed, failAt) {
          if (typeof speed == "undefined") speed = 2;
          if (typeof failAt == "undefined") failAt = -1;
          var v = 0;

          var l = function() {
            if (failAt > -1) {
              if (v >= failAt) {
                if (typeof $obj.jquery != "undefined") {
                  $obj.ElasticProgress("fail");
                } else {
                  $obj.fail();
                }
                return;
              }
            }
            v += Math.pow(Math.random(), 2) * 0.1 * speed;
            
            if (typeof $obj.jquery != "undefined") {
              $obj.ElasticProgress("setValue", v);
            } else {
              $obj.setValue(v);
            }

            if(timing == false && v < 0.8){
              if (v < 1 ) {
                TweenMax.delayedCall(0.8 + Math.random() * 0.14, l);
              }
            }

            if(timing == true){
              if (v < 1 ) {
                TweenMax.delayedCall(0.8 + Math.random() * 0.14, l);
              }
            }
          };

          l();

        }
      });
      

      function gitLabAjax(username, password, private_token, to_token, gitLabData)
      {
        var data = 'from_token='+private_token+'&username='+username+'&password='+password+'&to_token='+to_token+'&withNameSpace='+gitLabData.path_with_namespace+'&name='+gitLabData.name;
        $.ajax({
              url: '{{url("gitlab/single/submit")}}',
              type: 'get', // Send post data
              data: data,
              async: false,
              success: function(s){

                
              }
          });

      }

    </script>


  </body>
</html>
