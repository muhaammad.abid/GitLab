<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>GitLab</title>
		<meta name="description" content="Simple ideas for enhancing text input interactions" />
		<meta name="keywords" content="input, text, effect, focus, transition, interaction, inspiration, web design" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="{{url('assets/css/normalize.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{url('assets/fonts/font-awesome-4.2.0/css/font-awesome.min.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{url('assets/css/demo.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{url('assets/css/set2.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{url('assets/css/css/main.css')}}" />
		<!--[if IE]>
  		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body class="bgcolor-5">
		<div class="container">
		<form action="{{url('gitlab/submit')}}" method="post">

			<section class="content">
				<h1>Gitlab To Gitlab Import Easy <small>(All Projects)</small></h1>
        		<h3>from</h3>
				<span class="input input--kozakura">
					<input class="input__field input__field--kozakura" name="from_token" required="" type="text" id="input-1" />
					<label class="input__label input__label--kozakura" for="input-1">
						<span class="input__label-content input__label-content--kozakura" data-content="From Private Token">Private Token</span>
					</label>
					<svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						<path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
					</svg>
				</span>
				<span class="input input--kozakura">
					<input class="input__field input__field--kozakura" type="text" name="username" required="" id="input-2" />
					<label class="input__label input__label--kozakura" for="input-2">
						<span class="input__label-content input__label-content--kozakura"  data-content="Username">Username</span>
					</label>
					<svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						<path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
					</svg>
				</span>
				<span class="input input--kozakura">
					<input class="input__field input__field--kozakura" type="password" name="password" required="" id="input-3" />
					<label class="input__label input__label--kozakura" for="input-3">
						<span class="input__label-content input__label-content--kozakura" data-content="Password">Password</span>
					</label>
					<svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						<path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
					</svg>
				</span>
        		<h3>to</h3>
				<span class="input input--kozakura">
					<input class="input__field input__field--kozakura" type="text" name="to_token" required="" id="input-4" />
					<label class="input__label input__label--kozakura" for="input-4">
						<span class="input__label-content input__label-content--kozakura" data-content="To Private Token">Private Token</span>
					</label>
					<svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						<path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
					</svg>
				</span>
        	<br>
	        <button id="component-1" type="submit" class="button button--1">
	          Import
	          <span class="button__container">
	            <span class="circle top-left"></span>
	            <span class="circle top-left"></span>
	            <span class="circle top-left"></span>
	            <span class="button__bg"></span>
	            <span class="circle bottom-right"></span>
	            <span class="circle bottom-right"></span>
	            <span class="circle bottom-right"></span>
	          </span>
	        </button>


			</section>

		</form>


		</div><!-- /container -->
		<script src="{{url('assets/js/classie.js')}}"></script>
   		 <script src="{{url('assets/js/js/TweenMax.min.js')}}"></script>
		<script src="{{url('assets/js/js/main.js')}}"></script>
		<script>
			(function() {
			  // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
			  if (!String.prototype.trim) {
			    (function() {
			      // Make sure we trim BOM and NBSP
			      var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
			      String.prototype.trim = function() {
			        return this.replace(rtrim, "");
			      };
			    })();
			  }

			  [].slice
			    .call(document.querySelectorAll("input.input__field"))
			    .forEach(function(inputEl) {
			      // in case the input is already filled..
			      if (inputEl.value.trim() !== "") {
			        classie.add(inputEl.parentNode, "input--filled");
			      }

			      // events:
			      inputEl.addEventListener("focus", onInputFocus);
			      inputEl.addEventListener("blur", onInputBlur);
			    });

			  function onInputFocus(ev) {
			    classie.add(ev.target.parentNode, "input--filled");
			  }

			  function onInputBlur(ev) {
			    if (ev.target.value.trim() === "") {
			      classie.remove(ev.target.parentNode, "input--filled");
			    }
			  }
			})();
		</script>
	</body>
</html>
