$(document).ready(function() {
  $(".Download")
    .eq(0)
    .ElasticProgress({
      align: "center",
      colorFg: "#f9f8f7",
      colorBg: "#c5c5c5",
      highlightColor: "#ffab91",
      width: Math.min($(window).width() / 2 - 100, 600),
      barHeight: 10,
      labelHeight: 50,
      labelWobbliness: 150,
      bleedTop: 120,
      bleedRight: 100,
      buttonSize: 100,
      fontFamily: "Arvo",
      barStretch: 10,
      barInset: 4,
      barElasticOvershoot: 4,
      barElasticPeriod: 0.9,
      textFail: "Transfer Failed",
      textComplete: "Transfer Complete",
      arrowHangOnFail: true,
      arrowDirection: "up",
      onClick: function() {
        var notification = new NotificationFx({
          message:
            '<span class="icon icon-calendar"></span><p>The event was added to your calendar. Check out all your events in your <a href="#">event overview</a>.</p>',
          layout: "attached",
          effect: "bouncyflip",
          type: "notice", // notice, warning or error
          onClose: function() {
            //bttn.disabled = false;
          }
        });

        // show the notification
        notification.show();
        $(this).ElasticProgress("open");
      },
      onOpen: function() {
        fakeLoading($(this));
      },
      onComplete: function() {
        var $obj = $(this);

        TweenMax.delayedCall(1.5, function() {
          $obj.ElasticProgress("close");
        });
      }
    });

  function fakeLoading($obj, speed, failAt) {
    if (typeof speed == "undefined") speed = 2;
    if (typeof failAt == "undefined") failAt = -1;
    var v = 0;
    var l = function() {
      if (failAt > -1) {
        if (v >= failAt) {
          if (typeof $obj.jquery != "undefined") {
            $obj.ElasticProgress("fail");
          } else {
            $obj.fail();
          }
          return;
        }
      }
      v += Math.pow(Math.random(), 2) * 0.1 * speed;

      if (typeof $obj.jquery != "undefined") {
        $obj.ElasticProgress("setValue", v);
      } else {
        $obj.setValue(v);
      }
      if (v < 1) {
        TweenMax.delayedCall(0.05 + Math.random() * 0.14, l);
      }
    };
    l();
  }
});
