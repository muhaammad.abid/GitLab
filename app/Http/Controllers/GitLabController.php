<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class GitLabController extends Controller
{
	public function gitLabForm()
	{
		return view('index');
	}
	public function gitLabList(Request $request)
	{
		try {

    		$response = Curl::to('https://gitlab.com/api/v4/projects?visibility=private&per_page=100&private_token='.$request->from_token)
	        ->get();
	        $result = json_decode($response);
	        // dd(file_get_contents('https://gitlab.com/api/v4/projects?visibility=private&per_page=100&private_token=J2k2r_xNqrizyDDuN9yA'));

	        if($result){
	        	if(!isset($result->message)){
	        		
	        		$data['username'] = $request->username;
	        		$data['password'] = $request->password;
	        		$data['from_token'] = $request->from_token;
	        		$data['to_token'] = $request->to_token;
	        		$data['result'] = $result;

		        	return $data;
		        }else{
		        	return '401 Unauthorized!';
		        }

	        }else{
	        	return 'Something Wrong!';
	        }
    	} catch (Exception $e) {
    		return 'Something Wrong!';
    	}
	}

    public function gitLabFormSubmit(Request $request)
    {

    	try {
    		// $response = Curl::to('https://gitlab.com/api/v4/projects?visibility=private&per_page=100&private_token='.$request->from_token)
	     //    ->get();
	     //    $result = json_decode($response);
	     //    dd($result);
	     //    if($result->message != '401 Unauthorized'){
	     //    	foreach ($result as $key => $value) {

	    			$url = 'https://'.$request->username.':'.$request->password.'@gitlab.com/'.$request->withNameSpace.'.git';

	    			$gitlab = Curl::to('https://gitlab.com/api/v4/projects')
						        ->withData( array( 'import_url' => $url, 'name' => $request->name, 'private_token' => $request->to_token ) )
						        ->post();
					dd($gitlab);

	        	// }

	        	return 'Successfully';
	        // }else{
	        // 	return 'Something Wrong!';
	        // }
    	} catch (Exception $e) {
    		return 'Something Wrong!';
    	}
    }
}
